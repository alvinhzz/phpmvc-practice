<?php

class About extends Controller {

    public function index($nama = 'Aslamadin', $pekerjaan = 'Mahasiswa')
    {
        $this->view('about/index');
    }

    public function page()
    {
        $this->view('about/page');
    }
}